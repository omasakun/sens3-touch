This repository structure & settings are based on [s2t2s-v3](https://github.com/omasakun/s2t2s-v3/tree/29530bfcdd4ea8f9fa667bf6e84418f795068b3a)

## ファイル構成について
### エディター・タスクランナー・コンパイラーの設定ファイル
- `/.vscode/*` : VSCodeの設定ファイル
- `/tsconfig.json` : TypeScriptのコンパイラーのための設定ファイル
- `/gulpfile.js` : gulpというタスクランナーの設定ファイル。 sass, pugのコンパイルやミニファイをする作業を書いてある(TypeScriptのコンパイルについては書いていない)
- `/package.json` : コンパイルに必要なパッケージのリストなどが書かれている(pugファイルから参照されることがある)

### `src`から自動生成されるファイル
- `/docs/**/*` : `/src/**/*` からコンパイルされたファイル
- `/typedocs/**/*` : `/src/ts` から生成されたドキュメンテーションファイル。(TypeScriptのファイル中に書いたドキュメンテーションがまとめられている)

### 人が書いたソースコード
- `/src/pug/*.pug` : pugファイル。htmlにコンパイルされる
- `/src/pug/config.json` : pugファイルから参照されるJSONファイル
- `/src/sass/*.sass` : sassファイル。cssにコンパイルされる
- `/src/ts/*.ts` : TypeScriptファイル。jsにコンパイルされる

### その他
- `/License` : ライセンス
- `/README.md` : このファイル
- `/.gitignore` : Gitでの管理から除外するファイルのリスト

## ソースコードを書き換えたりして遊んでみたい
次の手順を踏んでください。

### 初回のみ: 必要なファイルのダウンロード
1. [Node.js](https://nodejs.org) のインストール (Linux, OS Xでは[nvm](https://github.com/creationix/nvm)  Windowsでは [nvm-windows](https://github.com/coreybutler/nvm-windows) を使うと後に面倒が起きなかったりする)
2. このレポジトリをダウンロード
3. ターミナル(Windowsでは、コマンドプロンプト)を開く
4. ダウンロードしたフォルダーをカレントディレクトリにする
5. (任意) `npm install -g pnpm` を実行し、[pnpm](https://github.com/pnpm/pnpm)を入れる
6. pnpmを入れたのであれば、`pnpm install`、そうでなければ`npm install` を実行
7. `npm install typescript` を実行
以上で、プログラムのコンパイルなどに必要なファイルが全て揃いました。

### 繰り返し: ビルド
1. ターミナル(Windowsでは、コマンドプロンプト)を開く
2. ダウンロードしたフォルダーをカレントディレクトリにする
3. `npm run dev` を実行 (sass, pugのコンパイル・css, jsのミニファイが行われる。`src`フォルダー内のファイルを書き換えるたびに自動読込されるブラウザーも立ち上げられる)
4. それと同時に、`npm run tsc` を実行 (typescriptのコンパイルが行われる) (gulpでtypescriptもコンパイルさせるとき、そのエラーレポートをVS Code に解釈させる方法を知らなかったため、 gulp に typescript のコンパイルを含めなかった)
5. 一段落したら、`npm run typedoc` を実行し、`typedocs` フォルダーの中身を自動生成

(dev, tsc, typedoc のコマンドは、VS Codeでは、`Tasks: Run Task`することで Configured Tasks としても表示されます。)

## Git Commands
備忘録
### Init
```
git init

git remote add github git@github.com:omasakun/sens3-touch.git
git remote add gitlab git@gitlab.com:omasakun/sens3-touch.git
git remote add sass-base git@gitlab.com:omasakun/lib-v2-sass-base.git
git remote add ts-browser git@gitlab.com:omasakun/lib-v2-ts-browser.git
git remote add ts-common git@gitlab.com:omasakun/lib-v2-ts-common.git

git subtree add --squash --prefix=src/sass/base sass-base master
git subtree add --squash --prefix=src/ts/lib ts-browser master
git subtree add --squash --prefix=src/ts/lib-common ts-common master

```

### Push/Pull library changes
```
git subtree push --squash --prefix=src/sass/base sass-base master
git subtree push --squash --prefix=src/ts/lib ts-browser master
git subtree push --squash --prefix=src/ts/lib-common ts-common master
```
```
git subtree pull --squash --prefix=src/sass/base sass-base master
git subtree pull --squash --prefix=src/ts/lib ts-browser master
git subtree pull --squash --prefix=src/ts/lib-common ts-common master
```