//#region imports
const gulp = require('gulp');
const watch = require('gulp-watch');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const sass = require('gulp-sass');
const sassGlob = require("gulp-sass-glob");
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const closureCompiler = require('google-closure-compiler').gulp( /*{ jsmode: true }*/ );
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const rollup = require('rollup-stream');
const commonjs = require("rollup-plugin-commonjs");
const resolve = require("rollup-plugin-node-resolve");
const includePaths = require("rollup-plugin-includepaths");
const fs = require('fs');
const sourcemaps = require('gulp-sourcemaps');
const runSequence = require('run-sequence');
const rollup_sourcemaps = require('rollup-plugin-sourcemaps');
const trash = require("trash");
//#endregion
// client
const cPath = {
	'src_pug': './src/pug/',
	'src_sass': './src/sass/',
	'pug_config_package': "./package.json",
	'pug_config_user': "./src/pug/config.json",
	'out_html': './docs/',
	'out_css': './docs/css/',
	'src_js': './tmp-ts-client/',
	'src_js_entry': "./tmp-ts-client/index.js",
	'out_js_bundle': "./docs/js/",
	'out_js_bundle_name': "bundle",
	'src_asset': './src/asset/',
	'out_asset': "./docs/asset/"
};
/*
const cPath = {
	'src_pug': './src/client/pug/',
	'src_sass': './src/client/sass/',
	'pug_config_package': "./package.json",
	'pug_config_user': "./src/client/pug/config.json",
	'out_html': './out/client/',
	'out_css': './out/client/css/',
	'src_js': './tmp-ts-client/',
	'src_js_entry': "./tmp-ts-client/client/ts/index.js",
	'out_js_bundle': "./out/client/js/",
	'out_js_bundle_name': "bundle",
	'src_asset': './src/client/asset/',
	'out_asset': "./out/client/asset/"
};
*/
// server
// Not used
const sPath = {
	'src_asset': './src/server/asset/',
	'out_asset': "./out/asset/"
};


//#region Variables for Gulp Tasks
const p = {
	client: {
		pug: {
			src: [cPath.src_pug + '**/*.pug', '!' + cPath.src_pug + '**/_*.pug'],
			dest: cPath.out_html,
			config_pkg: cPath.pug_config_package,
			config_usr: cPath.pug_config_user,
		},
		sass: {
			src: cPath.src_sass + '**/*.sass',
			dest: cPath.out_css
		},
		mincss: {
			src: [cPath.out_css + '**/*.css', '!' + cPath.out_css + '**/*.min.css'],
			dest: cPath.out_css
		},
		jsbundle: {
			src: cPath.src_js_entry,
			dest: cPath.out_js_bundle,
			dest_name: cPath.out_js_bundle_name + ".js",
			dest_name_min: cPath.out_js_bundle_name + ".min.js"
		},
		asset: {
			src: [cPath.src_asset + '**/*'],
			src_opt: {
				base: cPath.src_asset
			},
			dest: cPath.out_asset
		},
		watch: {
			pug: [cPath.src_pug + '**/*.*', cPath.pug_config_user, cPath.pug_config_package],
			sass: cPath.src_sass + '**/*.sass',
			js: [cPath.src_js + '**/*.js', '!' + cPath.src_js + '**/*.min.js'],
			asset: [cPath.src_asset + '**/*']
		}
	},
	browserSync: {
		baseDir: cPath.out_html
	},
	server: {
		asset: {
			src: [sPath.src_asset + '**/*'],
			src_opt: {
				base: sPath.src_asset
			},
			dest: sPath.out_asset
		},
		watch: {
			asset: [sPath.src_asset + '**/*']
		}
	},
};
//#endregion
//#region Options for external libraries
const pugLocals = {
	package: {},
	user: {}
}

const pugOptions = {
	locals: pugLocals
}

const sassOptions = {
	outputStyle: 'expanded'
}

const includePathOptions = {
	include: {},
	paths: [cPath.src_js_entry],
	external: [],
	extensions: ['.js', '.json']
}

const rollupOptions = {
	input: "", // set in client:jsbundle
	format: 'es',
	cache: cache,
	treeshake: true,
	sourcemap: true,
	plugins: [resolve(), commonjs(), includePaths(includePathOptions), rollup_sourcemaps()]
}

const closureOptions = {
	"compilation_level": "SIMPLE",
	"warning_level": "VERBOSE",
	"language_in": "ECMASCRIPT_2015",
	"language_out": "ECMASCRIPT_2015",
	"output_wrapper": "(function(){%output%})()",
	"js_output_file": cPath.out_js_bundle + cPath.out_js_bundle_name + ".min.js"
}

const plumberOptions = {
	errorHandler: notify.onError('Error: <%= error.message %>')
};

const autoprefixerOptions = ['> 3% in JP', 'ie 11', 'android 4.4', 'last 1 versions'];
//#endregion
//#region Gulp Tasks - Client
gulp.task('client:pug', () => {
	const $ = p.client.pug;

	pugLocals.package = JSON.parse(fs.readFileSync($.config_pkg).toString());
	pugLocals.user = JSON.parse(fs.readFileSync($.config_usr).toString());

	return gulp.src($.src)
		.pipe(plumber(plumberOptions))
		.pipe(pug(pugOptions))
		.pipe(gulp.dest($.dest));
});

gulp.task('client:sass', () => {
	const $ = p.client.sass;

	return gulp.src($.src)
		.pipe(plumber(plumberOptions))
		.pipe(sassGlob())
		.pipe(sourcemaps.init())
		.pipe(sass(sassOptions))
		.pipe(autoprefixer(autoprefixerOptions))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest($.dest));
});

gulp.task('client:mincss', () => {
	const $ = p.client.mincss;

	return gulp.src($.src)
		.pipe(plumber(plumberOptions))
		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest($.dest));
});

var cache;
gulp.task('client:jsbundle', () => {
	const $ = p.client.jsbundle;

	rollupOptions.input = $.src;

	return rollup(rollupOptions)
		.on('bundle', (bundle) => cache = bundle)
		.pipe(plumber(plumberOptions))
		.pipe(source($.dest_name))
		.pipe(buffer())
		.pipe(sourcemaps.init({
			loadMaps: true
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest($.dest))
		.pipe(closureCompiler(closureOptions))
		.pipe(rename($.dest_name_min))
		.pipe(gulp.dest($.dest));
});
gulp.task('client:asset', () => {
	const $ = p.client.asset;
	return gulp.src($.src, $.src_opt)
		.pipe(gulp.dest($.dest));
});

gulp.task('client:build:sass', callback => {
	runSequence("client:sass", "client:mincss", "reload", callback)
});
gulp.task('client:build:pug', callback => {
	runSequence("client:pug", "reload", callback)
});
gulp.task('client:build:js', callback => {
	runSequence("client:jsbundle", "reload", callback)
});
gulp.task('client:build:asset', callback => {
	runSequence("client:asset", "reload", callback)
});
gulp.task('client:build', ["client:build:sass", "client:build:pug", "client:build:js", "client:build:asset"]);
gulp.task('client:watch', () => {
	const $ = p.client.watch;

	watch($.pug, () => gulp.start("client:build:pug"));
	watch($.sass, () => gulp.start("client:build:sass"));
	watch($.js, () => gulp.start("client:build:js"));
	watch($.asset, () => gulp.start("client:build:asset"));
});

gulp.task('client', (callback) => {
	runSequence('browser-sync', 'client:build', 'client:watch', callback);
});
//#endregion
//#region Gulp Tasks - BrowserSync
gulp.task('browser-sync', () => {
	const $ = p.browserSync;

	browserSync.init({
		server: {
			baseDir: $.baseDir
		}
	});
});

gulp.task('reload', () => {
	if (browserSync.active) {
		browserSync.reload();
	}
});
//#endregion
//#region Gulp Tasks - server
/*
gulp.task('server:asset', () => {
	const $ = p.server.asset;
	return gulp.src($.src, $.src_opt)
		.pipe(gulp.dest($.dest));
});
gulp.task('server:build:asset', callback => {
	runSequence("server:asset", callback)
});
gulp.task('server:build', ["server:build:asset"]);
gulp.task('server:watch', () => {
	const $ = p.server.watch;

	watch($.asset, () => gulp.start("server:build:asset"));
});

gulp.task('server', (callback) => {
	runSequence('server:build', 'server:watch', callback);
});
*/
//#endregion

gulp.task('clean', (callback) => {
	trash([
		"tmp-ts-client",
		".sass-cache",
		"typedocs",
		"depgraph*.svg",
		"docs",
		"ts-declaration"
	]);
	callback();
});