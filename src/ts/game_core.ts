import { LList, LListItem } from "./linked_list";

/**
 * 描画関数群を表す型です。
 * ラッパーでもいいですし、描画コンテキストをそのまま渡してもいいです。
 * Renderer であることを明確に示すための型です。
 * KIND: 中身。レンダラー関数群本体
 * 例: const renderer = ctx as Renderer<CanvasRenderingContext2D>
 * 例: function fn<T>(renderer: Renderer<T>)
 * 例: function fn(renderer: Renderer<CanvasRenderingContext2D>)
 */
export type Renderer<KIND> = KIND & RendererI;
/** 
 * Renderer だということを示すためだけの型。
 * Nominal Typing を実現するための型。
 */
interface RendererI {
	_rendererBrand: undefined
}
export interface EMap {
	[id: string]: any
}

/**
 * ゲームに出てくるオブジェクトを表します。
 * オブジェクトの動作は Behav によって表されます。
 * Entity は、ツリー構造を形成します。
 * 変数の保持が主な役割です。
 * EMAP: EventMap。Entityが受け取ったり発信したりしうるイベントのリスト
 * 例: SceneManager, Entity
 */
export class Entity<EMAP extends EMap> {
	private parent: Entity<EMAP> | null = null
	private children: LList<Entity<EMAP>> = new LList()
	private behavs: LList<Behav<this, EMAP>> = new LList()
	private behavEventFnAfter: Map<keyof EMAP, LList<EMAP[any]>> = new Map() // TODO: More strong type check.
	private behavEventFnBefore: Map<keyof EMAP, LList<EMAP[any]>> = new Map() // TODO: More strong type check.
	private behav2eventFn: WeakMap<Behav<this, EMAP>, LList<LListItem<EMAP[any]>>> = new WeakMap();
	onEvent<K extends keyof EMAP>(kind: K, arg: EMAP[K]): this {
		const befores = this.behavEventFnBefore.get(kind);
		if (befores) {
			for (let ptr = befores.head; ptr; ptr = ptr.next) {
				ptr.value(this, arg);
			}
		}
		for (let ptr = this.children.head; ptr; ptr = ptr.next) {
			ptr.value.onEvent(kind, arg);
		}
		const afters = this.behavEventFnBefore.get(kind);
		if (afters) {
			for (let ptr = afters.head; ptr; ptr = ptr.next) {
				ptr.value(this, arg);
			}
		}
		return this;
	}
	addBehav(behav: Behav<this, EMAP>): this {
		if (behav.isRegistered) throw "This behav is already used to other Entity.";
		behav.isRegistered = true;
		this.behavs.append(behav);
		const behavEvents: LList<LListItem<EMAP[any]>> = new LList();
		behav.setEvents((timing, _kind, fn) => {
			const kind: keyof EMAP = _kind as any; // TODO: Type 'keyof EntityEMAP<this>' is not assignable to type 'keyof EMAP'
			const target = timing == "before" ? this.behavEventFnBefore : this.behavEventFnAfter;
			let eList = target.get(kind);
			if (!eList) {
				eList = new LList();
				target.set(kind, eList);
			}
			const eItem = eList.append(fn);
			behavEvents.append(eItem);
		});
		this.behav2eventFn.set(behav, behavEvents);
		return this;
	}
	removeBehav(behav: Behav<this, EMAP>): this {
		const events2remove = this.behav2eventFn.get(behav);
		if (!events2remove) throw "This behav is not registered to this Entity.";
		this.behav2eventFn.delete(behav);

		for (let ptr = events2remove.head; ptr; ptr = ptr.next) {
			ptr.value.remove(); // remove from this.behavEvents
		}

		this.behavs.removeValue(behav);
		behav.isRegistered = false;

		return this;
	}
	setParent(parent: Entity<EMAP>) {
		parent.addChild(this);
	}
	unsetParent(): this {
		if (!this.parent) throw "This Entity has no parent.";
		if (this.parent.children.removeValue(this) != 0) throw "BUG";
		this.parent = null;
		return this;
	}
	addChild(child: Entity<EMAP>): this {
		if (child.parent) {
			child.unsetParent();
		}
		this.children.append(child);
		return this;
	}
	removeChild(child: Entity<EMAP>): this {
		if (this.children.removeValue(child) != 0) throw "BUG";
		child.parent = null;
		return this;
	}
}
export type EntityEMap<E extends Entity<any>> = E extends Entity<infer EMAP> ? EMAP : never;
/** BeforeChildren / AfterChildren */
export type EventFnTiming = "before" | "after";
export type EventSetter<EMAP extends EMap, E extends Entity<EMAP>>
	= <K extends keyof EMAP>(timing: EventFnTiming, kind: K, fn: (entity: E, arg: EMAP[K]) => void) => void;

/**
 * Entity の動作を示します。
 * 必要であれば、プロパティを持つこともできます。
 * 例: FPSMeter, Bullet, TransitionEffect
 */
export abstract class Behav<E extends Entity<EMAP>, EMAP extends EMap> {
	/** This property is set by entity. DO NOT MODIFY this property. */
	isRegistered = false
	abstract setEvents(setEvent: EventSetter<EMAP, E>): void
}