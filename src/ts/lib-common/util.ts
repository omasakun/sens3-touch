export function dateFormat(date: Date): string {
	var y = date.getFullYear();
	var m = padLeft((date.getMonth() + 1).toString(), 2, " ");
	var d = padLeft(date.getDate().toString(), 2, " ");
	var h = padLeft(date.getHours().toString(), 2, " ");
	var min = padLeft(date.getMinutes().toString(), 2, " ");
	var s = padLeft(date.getSeconds().toString(), 2, " ");
	return y + "/" + m + "/" + d + " " + h + ":" + min + ":" + s;
}
export function timeFormat(date: Date): string {
	var h = padLeft(date.getHours().toString(), 2, " ");
	var min = padLeft(date.getMinutes().toString(), 2, " ");
	var s = padLeft(date.getSeconds().toString(), 2, " ");
	return h + ":" + min + ":" + s;
}
/** [min1,maxi] -> [min2,max2] */
export function mapValue(value: number, min1: number, max1: number, min2: number, max2: number) {
	return (value - min1) / (max1 - min1) * (max2 - min2) + min2;
}
export function showError2User(message: string): never {
	console.error(message);
	if (typeof (alert) == "function")
		alert(message);
	throw message;
}
export function strRight(str: string, len: number) {
	return str.substr(str.length - len);
}
export function padLeft(str: string, len: number, pad: string) {
	return pad.charAt(0).repeat(len - str.length) + str;
}
export interface Tree<T> {
	_: T,
	c?: Tree<T>[]
}

/** タプルとして配列の方を決定します。
 * ex) tuple([1, "A", { a: 2 }]) : [number, string, { a: number }]
 */
export function tuple<T extends [void] | {}>(n: T): T {
	return n;
}
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>

export function base64toBlob(base64: string, contentType: string = ""): Blob {
	const sliceSize = 1024;
	const asciiData = atob(base64);
	const len = asciiData.length;
	const sliceCount = Math.ceil(len / sliceSize);
	const byteArrays: Uint8Array[] = new Array(sliceCount);

	for (var sliceIndex = 0; sliceIndex < sliceCount; ++sliceIndex) {
		var begin = sliceIndex * sliceSize;
		var end = Math.min(begin + sliceSize, len);

		var bytes = new Array(end - begin);
		for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
			bytes[i] = asciiData[offset].charCodeAt(0);
		}
		byteArrays[sliceIndex] = new Uint8Array(bytes);
	}
	return new Blob(byteArrays, { type: contentType });
}