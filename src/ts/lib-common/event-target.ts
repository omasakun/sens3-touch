type Listener<THIS, EV> = (this: THIS, ev: EV) => any;
interface EMap {
	[type: string]: any
}
export class ETarget<THIS, EVENT_MAP extends EMap>  {
	private listeners: Map<string,
		{ listener: Listener<THIS, keyof EVENT_MAP>, isOnce: boolean }[]> = new Map();
	private readonly thisObject: THIS;
	constructor(thisObject: THIS) {
		this.thisObject = thisObject;
	}
	addEventListener<K extends keyof EVENT_MAP & string>
		(type: K, listener: Listener<THIS, EVENT_MAP[K]>, isOnce = false): void {
		if (this.listeners.has(type)) {
			this.listeners.get(type)!.push({ isOnce, listener });
		} else {
			this.listeners.set(type, [{ isOnce, listener }]);
		}
	}
	removeEventListener<K extends keyof EVENT_MAP & string>
		(type: K, listener: Listener<THIS, EVENT_MAP[K]>): { count: number } {
		if (!this.listeners.has(type)) return { count: 0 };
		const listeners = this.listeners.get(type)!;
		let count = 0;
		for (let i = listeners.length - 1; i >= 0; i--) {
			if (listeners[i].listener == listener) {
				listeners.splice(i, 1);
				count++;
			}
		}
		return { count };
	}
	dispatchEvent<K extends keyof EVENT_MAP & string>
		(type: K, event: EVENT_MAP[K]): { count: number } {
		if (!this.listeners.has(type)) return { count: 0 };
		const listeners = this.listeners.get(type)!.slice(); // listeners can be modified during running listener functions.
		const count = listeners.length;
		for (let i = 0; i < listeners.length; i++) {
			listeners[i].listener.call(this.thisObject, event);
		}
		const listeners2 = this.listeners.get(type)!; // listeners can be modified during running listener functions. 
		for (let i = listeners2.length - 1; i >= 0; i--) {
			if (listeners2[i].isOnce) {
				listeners2.splice(i, 1);
			}
		}
		return { count };
	}
}