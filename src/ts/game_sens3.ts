import { Vec2D } from "./geometric_obj";
import {
	Renderer as CoreRenderer,
	Entity as CoreEntity,
	Behav as CoreBehav,
	EventSetter as CoreEventSetter,
	EntityEMap as CoreEntityEMap
} from "./game_core";
import { FPS } from "./lib/fps";
//#region game_core Wrapper (Renderer, EMap, Entity, Behav, EventSetter, EntityEMap)

export type Renderer = CoreRenderer<CanvasRenderingContext2D>;

export interface EMap {
	pointerDown: { pos: Vec2D, time: number }
	pointerUp: { pos: Vec2D, time: number }
	pointerMove: { pos: Vec2D, time: number }

	resize: { oldSize: Vec2D, newSize: Vec2D }

	update: { time: number }
	draw: { time: number, renderer: Renderer }
}

//** Sens版 EMap を使った Entity を作るためのクラス) */
export class Entity extends CoreEntity<EMap>{
	/** tick イベントリスナーは 必ず この指示を守らなければならない */
	isEnable = true
	/** tick イベントリスナーは 必ず この指示を守らなければならない */
	isVisible = true
}

// @ts-ignore // TODO
//** Sens版 EMap を使った Behav を作るためのクラス) */
export abstract class Behav<E extends Entity> extends CoreBehav<E, EMap>{
	// Nothing
}

// @ts-ignore // TODO
export type EventSetter<E extends Entity> = CoreEventSetter<EMap, E>;

// @ts-ignore // TODO
export type EntityEMap<E extends Entity> = CoreEntityEMap<E>;

//#endregion

export function getRenderer(canvas: HTMLCanvasElement) {
	const ctx = canvas.getContext("2d");
	if (!ctx) return null;
	return ctx as Renderer;
}

export class VisibleEntity extends Entity {
	pos: Vec2D
	scale: Vec2D

	constructor(pos: Vec2D, scale: Vec2D) {
		super();
		this.pos = pos;
		this.scale = scale;
	}
}

export class FPSMeter extends Behav<Entity>{
	fps = new FPS()
	setEvents(setEvent: EventSetter<Entity>): void {
		setEvent("after", "update", e => {
			if (e.isEnable)
				this.fps.tick();
		});
	}
}

export class LinearMoveBehav extends Behav<Entity> {

}