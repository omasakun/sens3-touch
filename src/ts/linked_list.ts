export class LList<T>{
	head: LListItem<T> | null = null;
	tail: LListItem<T> | null = null;
	constructor(...values: T[]) {
		for (let i = 0; i < values.length; i++) {
			this.append(values[i]);
		}
	}
	prepend(value: T) {
		if (this.head) {
			return this.insBefore(this.head, value);
		} else {
			return this.head = this.tail = new LListItem(value);
		}
	}
	append(value: T) {
		if (this.tail) {
			return this.insAfter(this.tail, value);
		} else {
			return this.head = this.tail = new LListItem(value);
		}
	}
	insBefore(ptr: LListItem<T>, value: T) {
		const item = new LListItem(value);
		const prev = ptr.prev;
		if (prev) prev.next = item;
		ptr.prev = item;
		return item;
	}
	insAfter(ptr: LListItem<T>, value: T) {
		const item = new LListItem(value);
		const next = ptr.next;
		if (next) next.prev = item;
		ptr.next = item;
		return item;
	}
	find(value: T): LListItem<T> | null {
		for (let ptr = this.head; ptr; ptr = ptr.next) {
			if (ptr.value == value) return ptr;
		}
		return null;
	}
	findAll(value: T): LListItem<T>[] {
		const result: LListItem<T>[] = [];
		for (let ptr = this.head; ptr; ptr = ptr.next) {
			if (ptr.value == value) result.push(ptr);
		}
		return result;
	}
	removeValue(value: T): number {
		let count = 0;
		let ptr = this.head;
		while (ptr) {
			const next = ptr.next;
			if (ptr.value == value) {
				ptr.remove(); // ptr.next is modified here
				count++;
			}
			ptr = next;
		}
		return count;
	}
}
export class LListItem<T>{
	value: T
	prev: LListItem<T> | null;
	next: LListItem<T> | null;
	constructor(value: T) {
		this.value = value;
		this.prev = null;
		this.next = null;
	}
	remove() {
		const prev = this.prev;
		const next = this.next;
		if (prev) prev.next = next;
		if (next) next.prev = prev;
		this.prev = null;
		this.next = null;
	}
}