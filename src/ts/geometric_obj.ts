export class Vec2D {
	x: number
	y: number
	constructor(x: number, y: number) {
		this.x = x;
		this.y = y;
	}
	add(vec: Vec2D): this {
		this.x += vec.x;
		this.y += vec.y;
		return this;
	}
	sub(vec: Vec2D): this {
		this.x -= vec.x;
		this.y -= vec.y;
		return this;
	}
	mul(vec: Vec2D): this {
		this.x *= vec.x;
		this.y *= vec.y;
		return this;
	}
	mulNum(n: number): this {
		this.x *= n;
		this.y *= n;
		return this;
	}
	div(vec: Vec2D): this {
		this.x /= vec.x;
		this.y /= vec.y;
		return this;
	}
	divNum(n: number): this {
		this.x /= n;
		this.y /= n;
		return this;
	}
	eq(vec: Vec2D): boolean {
		return this.x == vec.x && this.y == vec.y;
	}
	neg(): this {
		this.x *= -1;
		this.y *= -1;
		return this;
	}
	dot(vec: Vec2D): number {
		return this.x * vec.x + this.y * vec.y;
	}
	unit(): this {
		this.divNum(this.len());
		return this;
	}
	len(): number {
		return Math.sqrt(this.x ** 2 + this.y ** 2);
	}
	setLen(l: number): this {
		this.mulNum(l / this.len());
		return this;
	}
	angle(): number {
		return Math.atan2(this.y, this.x);
	}
	setAngle(angle: number): this {
		const l = this.len();
		this.x = Math.cos(angle) * l;
		this.y = Math.sin(angle) * l;
		return this;
	}
	min(): number {
		return Math.min(this.x, this.y);
	}
	max(): number {
		return Math.max(this.x, this.y);
	}
	clone(): Vec2D {
		return new Vec2D(this.x, this.y);
	}
}
export class Rect {
	pos: Vec2D
	size: Vec2D
	constructor(pos: Vec2D, size: Vec2D) {
		this.pos = pos;
		this.size = size;
	}
	x() { return this.pos.x }
	y() { return this.pos.y }
	w() { return this.size.x }
	h() { return this.size.y }
	left() { return this.pos.x }
	right() { return this.pos.x + this.size.x }
	top() { return this.pos.y }
	bottom() { return this.pos.y + this.size.y }
	isHitRect(t: Rect) {
		return (t.left() < this.right()) && (this.left() < t.right())
			&& (t.top() < this.bottom()) && (this.top() < t.bottom());
	}
	isHitCircle(t: Circle) {
		return t.isHitRect(this);
	}
	clone() { return new Rect(this.pos.clone(), this.size.clone()) }
}
export class Circle {
	pos: Vec2D
	r: number
	constructor(pos: Vec2D, r: number) {
		this.pos = pos;
		this.r = r;
	}
	x() { return this.pos.x }
	y() { return this.pos.y }
	top() { return this.pos.y - this.r }
	bottom() { return this.pos.y + this.r }
	left() { return this.pos.x - this.r }
	right() { return this.pos.x + this.r }
	isHitCircle(t: Circle) {
		return pow2(this.x() - t.x()) + pow2(this.y() - t.y()) <= pow2(this.r + t.r);
	}
	isHitRect(rect: Rect) {
		const dx = this.x() - Math.max(rect.left(), Math.min(this.x(), rect.right()));
		const dy = this.y() - Math.max(rect.top(), Math.min(this.y(), rect.bottom()));
		return pow2(dx) + pow2(dy) <= pow2(this.r);
	}
	clone() { return new Circle(this.pos.clone(), this.r) }
}

function pow2(a: number) {
	return a * a;
}