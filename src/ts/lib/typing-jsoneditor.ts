import * as _jsoneditor from "jsoneditor";
export const JSONEditor: typeof _jsoneditor.default = window["JSONEditor"];
export type JSONEditorType = _jsoneditor.default;