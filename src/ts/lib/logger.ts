import { timeFormat, dateFormat } from "../lib-common/util";
const symbol_ShowAll = Symbol("Show all log");
const header_ShowAll = ":: ALL Log ::";
export type Logger = (title: string, ...objects: any[]) => void;
interface LogItem {
	texts: string[]
	time: Date
	timing: number
}
export function getLogger(logElm: HTMLTextAreaElement, maxLen: number, log2console = true): Logger {
	const log: {
		title: string | typeof symbol_ShowAll,
		contents: LogItem[]
	}[] = [
			{
				title: symbol_ShowAll,
				contents: []
			}
		];
	let currentLogIndex = 0;
	const updateView = () => {
		const currentLog = log[currentLogIndex];
		if (currentLog.title == symbol_ShowAll) {
			const logTexts = ([] as (LogItem & { title: string })[])
				.concat(...log.map(_ => {
					if (_.title == symbol_ShowAll) return [];
					return _.contents.map(__ => ({
						texts: __.texts,
						time: __.time,
						timing: __.timing,
						title: _.title as string
					}));
				}))
				.sort((a, b) => b.timing - a.timing)
				.map(a => {
					const timeText = timeFormat(a.time);
					const blanks = " ".repeat(timeText.length);
					const title = a.title;
					return ([] as string[]).concat(
						([] as string[]).concat(...a.texts.map(_ => _.split("\n")))
							.map((text, i) => (i == 0 ? timeText : blanks) + " [" + title + "] " + text)
					).join("\n");
				});
			const logText = logTexts.length == 0 ? "[Log hasn't been output]" : logTexts.join("\n");
			logElm.value = `${header_ShowAll} ${currentLogIndex + 1}/${log.length}\n${logText}`;
		} else if (currentLog.contents.length == 0) {
			logElm.value = `::[ ${currentLog.title} ]:: ${currentLogIndex + 1}/${log.length}\n[Log hasn't been output]`;
		} else {
			const logText = currentLog.contents
				.map(a => {
					const timeText = timeFormat(a.time);
					const blanks = " ".repeat(timeText.length);
					return ([] as string[]).concat(
						([] as string[]).concat(...a.texts.map(_ => _.split("\n")))
							.map((text, i) => (i == 0 ? timeText : blanks) + " " + text)
					).join("\n");
				})
				.join("\n");
			logElm.value = `::[ ${currentLog.title} ]:: ${currentLogIndex + 1}/${log.length}\n${logText}`;
		}
	}
	updateView();
	logElm.addEventListener("keydown", e => {
		if (e.code == "ArrowLeft") {
			if (log.length > 0) currentLogIndex = (currentLogIndex - 1 + log.length) % log.length;
			updateView();
			e.preventDefault();
		} else if (e.code == "ArrowRight") {
			if (log.length > 0) currentLogIndex = (currentLogIndex + 1) % log.length;
			updateView();
			e.preventDefault();
		}
	});
	return (title: string, ...objects: any[]) => {
		if (log2console) console.log(`[LOGGER: ${title}]`, objects.length == 1 ? objects[0] : objects);
		var outputLog = log.find(_ => _.title == title);
		if (outputLog == undefined) {
			console.log(`[LOGGER] < New log-group added: ${title}`);
			log.push({ title: title, contents: [] });
			currentLogIndex = log.length - 1;
			outputLog = log[currentLogIndex];
		}
		var output = outputLog.contents;
		const newLogItem: LogItem = {
			time: new Date(),
			timing: performance.now(),
			texts: []
		};
		for (let i = 0; i < objects.length; i++) {
			const obj = objects[i];
			var text2append = "";
			if (typeof obj == "string") text2append = obj;
			else if (typeof obj == "object") {
				for (const key in obj) if (obj.hasOwnProperty(key))
					text2append += `[${key}|${JSON.stringify(obj[key])}]`;
			} else text2append = JSON.stringify(obj);
			newLogItem.texts.push(text2append);
		}
		outputLog.contents.unshift(newLogItem);
		outputLog.contents = output.slice(0, maxLen);
		if (log[currentLogIndex].title == symbol_ShowAll ||
			currentLogIndex == log.findIndex(_ => _.title == title)) {
			updateView();
		}
		// logElm.value = output.slice(Math.max(0, output.length - maxLen)).join("\n");
	}
}