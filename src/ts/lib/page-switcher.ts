export abstract class PageSwitcherBase {
	private parent: HTMLElement | null = null;
	private childID: string | undefined = undefined;
	constructor(parent?: HTMLElement) {
		if (parent) this.setParent(parent);
	}
	setParent(parent: HTMLElement | null) {
		if (this.parent) {
			throw "Parent has been already set.";
		}
		if (!parent) {
			throw "Parent is null.";
		}
		this.parent = parent;
		this.verifyParent();
		this.update();
	}
	show(childID: string) {
		this.childID = childID;
		this.update();
	}
	private verifyParent() {
		if (!this.parent) throw "Parent is null.";
		let idSet = new Set<string>();
		for (let child of this.parent.children) {
			let id = child.id;
			if (id.length == 0) continue;
			if (idSet.has(id)) throw `Duplicated HTMLElement's ID: ${id}`;
			idSet.add(id);
		}
	}
	private update() {
		if (!this.parent) return;
		if (!this.childID) return;
		let found = false;
		for (let child of this.parent.children) {
			if (child.id == this.childID) {
				this._showElement(child);
				found = true;
			} else this._hideElement(child);
		}
		if (!found) throw `Child whose id is ${this.childID} is not found.`;
	}
	abstract _hideElement(element: Element)
	abstract _showElement(element: Element)
}
export class PageSwitcherSimple extends PageSwitcherBase {
	_hideElement(element: Element) {
		element.classList.add("hide");
	}
	_showElement(element: Element) {
		element.classList.remove("hide");
	}
}

// TODO: Add Animated Page Switcher