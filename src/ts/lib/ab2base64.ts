export function arrayBuffer2base64(arrayBuffer: ArrayBuffer): Promise<string> {
	const blob = new Blob([arrayBuffer], { type: 'application/octet-binary' });
	const reader = new FileReader();
	const result = new Promise<string>((res, rej) => {
		reader.onload = e => {
			const url = reader.result as (string | null);
			if (url)
				res(url.substr(url.indexOf(',') + 1));
			else
				rej("no data.");
		};
	});
	reader.readAsDataURL(blob);
	return result;
}