type onLoadListener = () => void;
export const onLoad = (() => {
	let isLoaded = false;
	let listeners: onLoadListener[] = [];
	window.addEventListener("load", () => {
		isLoaded = true;
		listeners.forEach(listener => {
			listener();
		});
		listeners = [];
	});
	return (listener: onLoadListener) => {
		if (isLoaded)
			setTimeout(listener, 0); // To make the execution order same as the ordinary event firing, do not execute immediately. It is similar to Promise.
		else
			listeners.push(listener);
	}
})();


export function loadScript(src: string): Promise<{}> {
	var done = false;
	var head = document.getElementsByTagName("head")[0];
	var script = document.createElement("script");
	script.src = src;
	head.appendChild(script);
	return new Promise((res) => {
		script.onload = () => {
			// @ts-ignore				
			if (!done && (!script.readyState || script.readyState === "loaded" || script.readyState === "complete")) {
				done = true;
				res();
				script.onload = null;
				if (head && script.parentNode)
					head.removeChild(script);
			}
		}
	});
}

export function URLtoObject(): { [id: string]: string } {
	var arg = {};
	var pair = location.search.substring(1).split("&");
	pair.forEach(function (V) {
		var kv = V.split("=");
		arg[kv[0]] = kv[1];
	});
	return arg;
}

export function downloadText(fileName: string, text: string) {
	downloadBlob(fileName, new Blob([text], { type: "text/plain" }));
}

export function downloadBlob(fileName: string, blob: Blob) {
	downloadByURL(fileName, window.URL.createObjectURL(blob));
}

export function downloadByURL(fileName: string, url: string) {
	var a = document.createElement("a");
	a.href = url
	a.download = fileName;
	a.style.display = "none";
	document.body.appendChild(a);
	a.click();
	document.body.removeChild(a);
}

export function loadFileAsText(): Promise<[string, FileReader]> {
	return new Promise((res, rej) => {
		var input = document.createElement("input");
		input.type = "file";
		input.addEventListener("change", (e) => {
			if (!(<any>e.target).files) rej();
			var file = (<any>e.target).files[0];
			if (!file) rej();
			var reader = new FileReader;
			reader.onload = (e) => {
				return res([<string>reader.result, reader]);
			};
			reader.readAsText(file);
		});
		document.body.appendChild(input);
		input.click();
		document.body.removeChild(input);
	});
}

export function loadFileAsDataURL(): Promise<[string, FileReader]> {
	return new Promise((res, rej) => {
		var input = document.createElement("input");
		input.type = "file";
		input.addEventListener("change", (e) => {
			if (!(<any>e.target).files) rej();
			var file = (<any>e.target).files[0];
			if (!file) rej();
			var reader = new FileReader;
			reader.onload = (e) => {
				return res([<string>reader.result, reader]);
			};
			reader.readAsDataURL(file);
		});
		document.body.appendChild(input);
		input.click();
		document.body.removeChild(input);
	});
}

/** @param loop An integer greater than or equal to 0 is given to ticks. Return true to quit */
export function eventloop(load: () => void, loop: (ticks: number, time: number) => boolean) {
	var cb = () => {
		load();
		var ticks = 0;
		var startTime = performance.now();
		setTimeout(function tmp() {
			if (loop(ticks, performance.now() - startTime)) return;
			ticks++;
			requestAnimationFrame(tmp);
		}, 0);
	}
	onLoad(cb);
}
