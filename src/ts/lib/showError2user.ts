import { showError2User } from "../lib-common/util";

window.onerror = function (msg, url, lineNo, columnNo, error) {
	var string = msg.toString().toLowerCase();
	var substring = "script error";
	if (string.indexOf(substring) > -1) {
		alert('Script Error: See Browser Console for Detail');
	} else {
		var message = [
			'Message: ' + msg,
			'URL: ' + url,
			'Line: ' + lineNo,
			'Column: ' + columnNo,
			'Error object: ' + JSON.stringify(error)
		].join(' - ');
		showError2User(message);
	}
	return false;
};