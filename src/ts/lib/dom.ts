export function ge(id: string) { return document.getElementById(id)!; }
export function setClassList(e: HTMLElement, remove: string[] = [], add: string[] = []) {
	for (var i = 0; i < remove.length; i++)e.classList.remove(remove[i]);
	for (var i = 0; i < add.length; i++)e.classList.add(add[i]);
}
export function setClass2Elms(className: string, remove: HTMLElement[] = [], add: HTMLElement[] = []) {
	for (let i = 0; i < remove.length; i++)remove[i].classList.remove(className);
	for (let i = 0; i < add.length; i++)add[i].classList.add(className);
}
export function ce<K extends keyof HTMLElementTagNameMap>(tagName: K, options?: { id?: string, classList?: string[], children?: HTMLElement[] ,innerText?:string}): HTMLElementTagNameMap[K] {
	var result = document.createElement(tagName);
	if (options) {
		if (options.id) result.id = options.id;
		if (options.classList) options.classList.forEach(_ => result.classList.add(_));
		if (options.children) options.children.forEach(_ => result.appendChild(_));
		if (options.innerText) result.innerText = options.innerText;

	}
	return result;
}