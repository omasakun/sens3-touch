# Defined Styles

## base
```
$cc-white
$cc-light
$cc-light-grey
$cc-dark-grey
$cc-dark
$cc-black

== Classes ==
  fullH fullW
  cV cH grow shrink
  scrollX scrollY
  listV listH hide stock
  cSpace cCenter
  center left right
  framed framed-block

@mixin ee-link($bg, $fg)
@mixin ee-input($bg, $fg, $border, $bg-hover, $fg-hover)
@mixin ee-outline-button($bg,$fg,$border,$bg-hover,$fg-hover,$fg-active)
@mixin ee-dotted-hr($border)
@mixin ee-selectable-li($fg-active,$bg-selected,$fg-selected)
```