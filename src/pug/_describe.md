このプロジェクトはあくまでもテンプレートです。

どこまで改変して使っても構いません。

-----

Tips: この説明文は Markdown を使って書かれているため、

- このような
- リストを追加することも、
- ul, li タグを書くことなく **かんたんに**

行うことができます。